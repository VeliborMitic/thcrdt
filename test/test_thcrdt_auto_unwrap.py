import json

import pytest

from thcrdt.thcrdt import Counter, _CRDT_List_Object, _CRDT_Dict_Object
from thcrdt.unwrap import CRDT

from thcrdt.thcrdt import _CRDT_Counter


def test_crdt_0():
    '''
    Test concurrent changes
    '''
    crdt = CRDT()
    doc1s: dict = crdt.from_({'cards': [{}]})
    assert doc1s == {'cards': [{}]}

    # client A
    def doc1a_cb(doc: dict):
        doc['cards'][0]['x'] = 0
        doc['cards'][0]['y'] = 10
        doc['cards'][0]['z'] = 20
    
    doc1a: dict = crdt.change(crdt.clone(doc1s), doc1a_cb)
    assert doc1a == {'cards': [{'x': 0, 'y': 10, 'z': 20}]}
    doc1a_changes: list = crdt.get_changes(crdt.clone(doc1s), doc1a)

    # client B
    def doc1b_cb(doc: dict):
        doc['cards'][0]['x'] = 100
        doc['cards'][0]['y'] = 110
        doc['cards'][0]['z'] = 120
    
    doc1b: dict = crdt.change(crdt.clone(doc1s), doc1b_cb)
    assert doc1b == {'cards': [{'x': 100, 'y': 110, 'z': 120}]}
    doc1b_changes: list = crdt.get_changes(crdt.clone(doc1s), doc1b)

    # apply changes
    doc1s_a, patch = crdt.apply_changes(crdt.clone(doc1s), doc1a_changes)
    assert doc1s_a == {'cards': [{'x': 0, 'y': 10, 'z': 20}]}

    # FIXME: Changes from client B are some times successfully applied to doc1s and sometimes not!!!
    doc1s_b, patch = crdt.apply_changes(crdt.clone(doc1s), doc1b_changes)
    assert doc1s_b == {'cards': [{'x': 100, 'y': 110, 'z': 120}]}


def test_crdt_1():
    '''
    Test concurrent changes
    '''
    crdt = CRDT()
    doc1s: dict = crdt.from_({'cards': [{}]})
    assert doc1s == {'cards': [{}]}

    # client A
    def doc1a_cb(doc: dict):
        doc['cards'][0]['x'] = 0
        doc['cards'][0]['y'] = 10
        doc['cards'][0]['z'] = 20

    doc1a: dict = crdt.change(crdt.clone(doc1s), doc1a_cb)
    assert doc1a == {'cards': [{'x': 0, 'y': 10, 'z': 20}]}

    doc1a_changes: list = crdt.get_changes(doc1s, doc1a)

    doc1s, patch = crdt.apply_changes(doc1s, doc1a_changes)
    assert doc1s == {'cards': [{'x': 0, 'y': 10, 'z': 20}]}

    # client B
    def doc1b_cb(doc: dict):
        del doc['cards'][0]['y']
        del doc['cards'][0]['z']
        doc['cards'][0]['w'] = 300

    doc1b: dict = crdt.change(crdt.clone(doc1s), doc1b_cb)
    assert doc1b == {'cards': [{'x': 0, 'w': 300}]}

    doc1b_changes: list = crdt.get_changes(doc1s, doc1b)

    doc1s, patch = crdt.apply_changes(doc1s, doc1b_changes)

    # client C
    def doc1c_cb(doc: dict):
        card = {
            'a': -1,
            'b': -2,
            'items': [],
        }

        doc['cards'].append(card)

    doc1c: dict = crdt.change(crdt.clone(doc1s), doc1c_cb)
    assert doc1c == {'cards': [{'x': 0, 'w': 300}, {'a': -1, 'b': -2, 'items': []}]}

    doc1c_changes: list = crdt.get_changes(doc1s, doc1c)

    doc1s, patch = crdt.apply_changes(doc1s, doc1c_changes)
    assert doc1s == {'cards': [{'x': 0, 'w': 300}, {'a': -1, 'b': -2, 'items': []}]}


def test_crdt_2():
    '''
    Test concurrent changes
    '''
    crdt = CRDT()
    doc1s: dict = crdt.from_({'cards': [{}]})
    assert doc1s == {'cards': [{}]}

    # client A
    def doc1a_cb(doc: dict):
        card = {
            'a': -1,
            'b': -2,
        }

        doc['cards'].append(card)

    doc1a: dict = crdt.change(crdt.clone(doc1s), doc1a_cb)
    assert doc1a == {'cards': [{}, {'a': -1, 'b': -2}]}

    doc1a_changes: list = crdt.get_changes(doc1s, doc1a)

    doc1s, patch = crdt.apply_changes(doc1s, doc1a_changes)
    assert doc1s == {'cards': [{}, {'a': -1, 'b': -2}]}

    # client B
    def doc1b_cb(doc: dict):
        card = {
            'a': -10,
            'b': -20,
        }

        doc['cards'].append(card)

    doc1b: dict = crdt.change(crdt.clone(doc1s), doc1b_cb)
    assert doc1b == {'cards': [{}, {'a': -1, 'b': -2}, {'a': -10, 'b': -20}]}

    doc1b_changes: list = crdt.get_changes(doc1s, doc1b)

    doc1s, patch = crdt.apply_changes(doc1s, doc1b_changes)
    assert doc1s == {'cards': [{}, {'a': -1, 'b': -2}, {'a': -10, 'b': -20}]}

    # client C
    def doc1c_cb(doc: dict):
        card = doc['cards'].pop()
        card = doc['cards'].shift()

    doc1c: dict = crdt.change(crdt.clone(doc1s), doc1c_cb)
    assert doc1c == {'cards': [{'a': -1, 'b': -2}]}

    doc1c_changes: list = crdt.get_changes(doc1s, doc1c)

    doc1s, patch = crdt.apply_changes(doc1s, doc1c_changes)
    assert doc1s == {'cards': [{'a': -1, 'b': -2}]}


def test_crdt_3():
    '''
    Test concurrent changes
    '''
    crdt = CRDT()

    doc: dict = {'cards': [
        {
            'a': Counter(10),
            'b': Counter(20),
        }
    ]}

    doc1s: dict = crdt.from_(doc)
    assert doc1s == {'cards': [{'a': _CRDT_Counter(10), 'b': _CRDT_Counter(20)}]}

    # client A
    def doc1a_cb(doc: dict):
        doc['cards'][0]['a'].increment(2)
        doc['cards'][0]['b'].decrement(2)

    doc1a: dict = crdt.change(crdt.clone(doc1s), doc1a_cb)
    assert doc1a == {'cards': [{'a': _CRDT_Counter(12), 'b': _CRDT_Counter(18)}]}

    doc1a_changes: list = crdt.get_changes(doc1s, doc1a)

    doc1s, patch = crdt.apply_changes(doc1s, doc1a_changes)
    assert doc1s == {'cards': [{'a': _CRDT_Counter(12), 'b': _CRDT_Counter(18)}]}


def test_crdt_dumps_isinstance_nested_dict_instance():
    '''
    Testing dumps function by given nested dict instance - success expected
    '''
    crdt = CRDT()
    doc1s: dict = crdt.from_({'cards': {}})
    assert doc1s == {'cards': {}}

    # client A
    def doc1a_cb(doc: dict):
        doc['cards'] = {'test': 1}

    doc1a: dict = crdt.change(crdt.clone(doc1s), doc1a_cb)
    assert doc1a == {'cards': {'test': 1}}

    doc1a_changes: list = crdt.get_changes(doc1s, doc1a)

    doc1s, patch = crdt.apply_changes(doc1s, doc1a_changes)
    assert doc1s == {'cards': {'test': 1}}


def test_crdt_list_obj_given_root_none():
    '''
    Testing creating instances for _CRDT_List_Object object by given None as root - success expected
    '''
    crdt_list_obj: _CRDT_List_Object = _CRDT_List_Object(root=None, parent=_CRDT_Dict_Object)
    assert isinstance(crdt_list_obj, _CRDT_List_Object)


def test_crdt_list_obj_given_list_instance_for_args():
    '''
    Testing _CRDT_List_Object object initialization by given instance of list for args - success expected
    '''
    crdt = CRDT()

    doc: dict = {'cards': [
        []
    ]}

    doc1s: dict = crdt.from_(doc)
    assert isinstance(doc1s, dict)
    assert doc1s == {'cards': [[]]}


def test_crdt_list_dumps():
    '''
    Testing crdt_list dumps function - success expected
    '''
    crdt_list_obj1: _CRDT_List_Object = _CRDT_List_Object(root=None, parent=_CRDT_Dict_Object)
    crdt_list_obj: _CRDT_List_Object = _CRDT_List_Object(root=crdt_list_obj1, parent=_CRDT_Dict_Object)

    doc: list = ['test1', 'test2', 'test3']
    crdt_list_obj.extend(doc)
    doc1_json = crdt_list_obj.dumps()

    doc1 = json.loads(doc1_json)
    assert doc1 == doc


def test_crdt_merge():
    '''
    Testing merge function - success expected
    '''
    crdt = CRDT()
    doc1s: dict = crdt.from_({'cards': [{}]})
    assert doc1s == {'cards': [{}]}

    # client A
    def doc1a_cb(doc: dict):
        doc['cards'][0]['x'] = 0
        doc['cards'][0]['y'] = 10
        doc['cards'][0]['z'] = 20

    doc1a: dict = crdt.change(crdt.clone(doc1s), doc1a_cb)
    assert doc1a == {'cards': [{'x': 0, 'y': 10, 'z': 20}]}

    # client B
    def doc1b_cb(doc: dict):
        doc['cards'][0]['x'] = 100
        doc['cards'][0]['y'] = 110
        doc['cards'][0]['z'] = 120
        doc['cards'][0]['w'] = 130

    doc1b: dict = crdt.change(crdt.clone(doc1a), doc1b_cb)
    assert doc1b == {'cards': [{'w': 130, 'x': 100, 'y': 110, 'z': 120}]}

    final_doc = crdt.merge(doc1a, doc1b)
    assert final_doc.items() == doc1b.items()
    assert final_doc == {'cards': [{'w': 130, 'x': 100, 'y': 110, 'z': 120}]}


def test_crdt_dict_delete_non_existing_key():
    '''
    Testing __delitem__ function for _CRDT_Dict_Object - KeyError expecting
    '''
    crdt = CRDT()
    doc: dict = crdt.from_({'cards': [{'x': 'letter x'}]})
    assert doc == {'cards': [{'x': 'letter x'}]}

    # client A
    def client_a(doc: dict):
        del doc['cards'][0]['a']

    # FIXME: crdt.change() does not raise KeyError when non-existing key passed;
    doc_a: dict = crdt.change(crdt.clone(doc), client_a)
    assert doc_a == {'cards': [{'x': 'letter x'}]}


def test_crdt_list_object_delete_item_at_valid_index():
    '''
    Testing __delitem__ function for _CRDT_List_Object - success expected
    '''
    crdt = CRDT()
    doc: dict = crdt.from_({'cards': [{'x': 'letter x'}]})
    assert doc == {'cards': [{'x': 'letter x'}]}

    # client A
    def client_a(doc: dict):
        del doc['cards'][0]

    doc_a: dict = crdt.change(crdt.clone(doc), client_a)
    assert doc_a == {'cards': []}


@pytest.mark.xfail(raises=Exception, strict=True)
def test_crdt_list_object_delete_item_non_valid_index():
    '''
    Testing __delitem__ function for _CRDT_List_Object - success expected
    '''
    crdt = CRDT()
    doc: dict = crdt.from_({'cards': [{'x': 'letter x'}]})
    assert doc == {'cards': [{'x': 'letter x'}]}

    # client A
    def client_a(doc: dict):
        del doc['cards'][2]

    doc_a: dict = crdt.change(crdt.clone(doc), client_a)
    assert doc_a == {'cards': []}


def test_crdt_list_object_set_item():
    '''
    Testing __setitem__ function for _CRDT_List_Object object - success expected
    '''
    crdt = CRDT()
    doc: dict = crdt.from_({'cards': [{'x': 'letter x'}]})
    assert doc == {'cards': [{'x': 'letter x'}]}

    # client A
    def client_a(doc: dict):
        doc['cards'][0] = {'y': 2}

    doc_a: dict = crdt.change(crdt.clone(doc), client_a)
    assert doc_a == {'cards': [{'y': 2}]}


def test_crdt_list_object_counter():
    '''
    Testing counter for dumps function - success expected
    '''
    crdt = CRDT()
    counter = Counter(1)
    doc: dict = crdt.from_({'cards': [counter]})
    assert doc == {'cards': [_CRDT_Counter(1)]}

    # client A
    def client_a(doc: dict):
        doc['cards'][0].increment(2)

    doc_a: dict = crdt.change(crdt.clone(doc), client_a)
    assert doc_a == {'cards': [_CRDT_Counter(3)]}


def test_crdt_list_obj_given_list():
    '''
    Testing _CRDT_List_Object object initialization by given instance of list for args - success expected
    '''
    crdt = CRDT()
    doc1s: dict = crdt.from_({'cards': [[1, 2, 3]]})
    assert isinstance(doc1s, dict)
    assert doc1s == {'cards': [[1, 2, 3]]}
