FROM python:3.10.8-alpine3.16 as base
RUN apk add --no-cache build-base linux-headers bsd-compat-headers musl-dev openssl-dev libffi-dev git curl rust cargo

FROM base AS builder
RUN mkdir -p /build
WORKDIR /build
COPY /test/requirements.txt /build/requirements.txt
RUN pip install --prefix=/build -r /build/requirements.txt

FROM base
COPY --from=builder /build /usr/local
COPY /thcrdt /deps/thcrdt
COPY /vendor /deps/vendor
COPY /test /code
WORKDIR /code
ENV PYTHONPATH=/deps
CMD coverage run -m pytest -v -s --maxfail=1 ; coverage report -m --fail-under=66
