from thcrdt.thcrdt import CRDT

crdt = CRDT()

# Create document with initial state
doc1s: dict = crdt.from_({'cards': [{}]}).unwrap()


# CLIENT A
# changes on client A
def doc_ca(doc: dict):
    card = {
        'a': -1,
        'b': -2,
    }

    doc['cards'].append(card)


# Use crdt.change() to update doc1s.
doc1a: dict = crdt.change(crdt.clone(doc1s).unwrap(), doc_ca).unwrap()

# Get changes made on client A.
doc1a_changes: list = crdt.get_changes(doc1s, doc1a).unwrap()

# Update initial document with changes 
doc1s, patch = crdt.apply_changes(doc1s, doc1a_changes).unwrap()


# CLIENT B
# changes on client B
def doc_cb(doc: dict):
    card = {
        'a': -10,
        'b': -20,
    }

    doc['cards'].append(card)


doc1b: dict = crdt.change(crdt.clone(doc1s).unwrap(), doc_cb).unwrap()

doc1b_changes: list = crdt.get_changes(doc1s, doc1b).unwrap()

doc1s, patch = crdt.apply_changes(doc1s, doc1b_changes).unwrap()


# CLIENT C
# changes on client C
def doc_cc(doc: dict):
    card = doc['cards'].pop()
    card = doc['cards'].shift()


doc1c: dict = crdt.change(crdt.clone(doc1s).unwrap(), doc_cc).unwrap()

doc1c_changes: list = crdt.get_changes(doc1s, doc1c).unwrap()

doc1s, patch = crdt.apply_changes(doc1s, doc1c_changes).unwrap()
